<?php 

/**
 * Write a PHP function that takes a string as input 
 * and returns the reversed version of that string. 
 * Avoid using built-in functions like strrev().
 */

function reverseString($input) {
	$reversed = '';
	$length = strlen($input);

	for ($i = $length - 1; $i >= 0; $i--) {
			$reversed .= $input[$i];
	}

	return $reversed;
}

// Example usage:
$inputString = "Hello, World!";
$reversedString = reverseString($inputString);
echo $reversedString;
