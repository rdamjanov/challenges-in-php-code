<?php 

/**
 * Write a PHP function that takes a string as input 
 * and returns the reversed version of that string. 
 * Avoid using built-in functions like strrev().
 */

function reverseString($input) {
	// Solution code goes here.
}

// Example usage:
$inputString = "Hello, World!";
$reversedString = reverseString($inputString);
echo $reversedString;
