<?php

/**
 * Write a PHP function that takes an array of integers as input 
 * and calculates the sum of all even numbers in the array.
 */

function sumOfEvenNumbers($numbers) {
    // Write solution here.
}

// Example usage:
$numbers = [1, 2, 3, 4, 5, 6];
$result = sumOfEvenNumbers($numbers);
echo $result;  // Output: 12
